'use strict';

// Issues controller
angular.module('issues').controller('IssuesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',
	function($scope, $stateParams, $location, Authentication, Issues) {
		$scope.authentication = Authentication;
    $scope.currentPage = 1;
    $scope.pageSize = 2;
    $scope.offset = 0;

		// Page changed handler
		$scope.pageChanged = function() {
			 $scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
		};

		// Create new Issue
		$scope.create = function() {
			// Create new Issue object
			var issue = new Issues ({
				name: this.name,
				description: this.description,
				sourceUrl: this.sourceUrl
			});

			// Redirect after save
			issue.$save(function(response) {
				$location.path('issues/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Issue
		$scope.remove = function(issue) {
			if ( issue ) {
				issue.$remove();

				for (var i in $scope.issues) {
					if ($scope.issues [i] === issue) {
						$scope.issues.splice(i, 1);
					}
				}
			} else {
				$scope.issue.$remove(function() {
					$location.path('issues');
				});
			}
		};

		// Update existing Issue
		$scope.update = function() {
			var issue = $scope.issue;

			issue.$update(function() {
				$location.path('issues/' + issue._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Issues
		$scope.find = function() {
			$scope.issues = Issues.query();
		};

		// Find existing Issue
		$scope.findOne = function() {
			$scope.issue = Issues.get({
				issueId: $stateParams.issueId
			});
		};
	}
]);
