'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	/**
	 * Validation
	 */
	function validateLength (v) {
	  // a custom validation function for checking string length to be used by the model
	  return v.length <= 40;
	}

/**
 * Issue Schema
 */
var IssueSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Issue name',
		trim: true,
		validate: [validateLength, 'name must be 40 chars in length or less']
	},
	description: {
		type: String,
		default: '',
		required: 'Please provide a brief summary',
		trim: true
	},
	sourceUrl: {
		type: String,
		default: '',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Issue', IssueSchema);
